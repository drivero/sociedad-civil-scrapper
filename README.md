# Sociedad Civil Scrapper 

## URLs para mapear:
- [Niñas, niños y adolescentes](http://www.mapeosociedadcivil.uy/organizaciones/ninas-ninos-y-adolescentes)

Para filtrar por Montevideo como departamento:

```URL?departamentos[]=montevideo&submit=Filtrar+departamento```

## Selectores:
En página de busqueda:
.wrapper > .row.blog > a => :attr(href)

En página de detalle
.wrapper > .container:last-child > .md-margin-top-20
Iterar y buscar siguiente info:
- Nombre
- Dirección
- Teléfono
- Email
- Web
- Referentes
- Resumen
- Tipo
- Area de trabajo
- Subareas de trabajo